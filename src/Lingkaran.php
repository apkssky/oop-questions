<?php 

class Lingkaran extends Luas implements BangunDatar
{
    const pi = 3.14;
    private $jarijari;
    
    public function __construct( $jarijari, $calInserted) {
        
        $this->jarijari = $jarijari;
        $this->cal = $calInserted;
	}

    public function getPi()
    {
        return $this->pi;
    }

    public function getJarijari()
    {
        return $this->jarijari;
    }

    public function hitungLuas(){
    	switch ($this->cal) {
            case "luaslingkaran":
                $result = (self::pi*pow($this->jarijari, 2));
                break;
            case "klllingkaran":
            	$result = (self::pi*($this->jarijari*2));
            	break;
            default:
                include_once 'index.php';
                break;
        }
        return $result;
    	
    }


    public function hasil(){
    	return "Hasil luas Lingkaran = ";
    }

}

 ?>