<?php 

class Segitiga extends Luas implements BangunDatar
{
    private $sisi;
    private $alas;
    private $tinggi;
    
    public function __construct($sisi, $alas, $tinggi, $calInserted) {
        $this->sisi = $sisi;
        $this->alas = $alas;
        $this->tinggi = $tinggi;
        $this->cal = $calInserted;
	}

    public function getAlas()
    {
        return $this->alas;
    }

    public function getTinggi()
    {
        return $this->tinggi;
    }

    public function hitungLuas(){
        switch ($this->cal) {
            case "luassegitiga":
                $result = (0.5*$this->alas*$this->tinggi);
                break;
            case "kllsegitiga":
                $result = ($this->sisi+$this->sisi+$this->sisi);
                break;
            
            default:
                include_once 'index.php';
                break;
        }
        return $result;
    	
    }


    public function hasil(){
    	return "Hasil luas segitiga = ";
    }

}





 ?>