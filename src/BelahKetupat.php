<?php 

class BelahKetupat extends Luas implements BangunDatar
{
    private $sisi;
    private $d1;
    private $d2;
    
    public function __construct($sisi,$d1, $d2, $calInserted) {
        $this->sisi = $sisi;
        $this->d1 = $d1;
        $this->d2 = $d2;
        $this->cal = $calInserted;
	}

    public function getD1()
    {
        return $this->d1;
    }

    public function getD2()
    {
        return $this->d2;
    }

    public function hitungLuas(){
        switch ($this->cal) {
            case "luasbelahketupat":
                $result = (0.5*$this->d1*$this->d2);
                break;
            case "kllbelahketupat":
                $result = ($this->sisi*4);
                break;
            
            default:
                include_once 'index.php';
                break;
        }
        return $result;
    	
    }


    public function hasil(){
    	return "Hasil luas belah ketupat = ";
    }

}





 ?>