<?php 

class PersegiPanjang extends Luas implements BangunDatar
{
    private $panjang;
    private $lebar;
    
    public function __construct($panjang, $lebar, $calInserted) {
        $this->panjang = $panjang;
        $this->lebar = $lebar;
        $this->cal = $calInserted;
	}

    public function getPanjang()
    {
        return $this->panjang;
    }

    public function getLebar()
    {
        return $this->lebar;
    }

    public function hitungLuas(){
        switch ($this->cal) {
            case "luaspersegipanjang":
                $result = ($this->panjang*$this->lebar);
                break;
            case "kllpersegipanjang":
                $result = (2*($this->panjang+$this->lebar));
                break;
            default:
                include_once 'index.php';
                break;
        }
        return $result;
    	
    }


    public function hasil(){
    	return "Hasil luas persegi panjang = ";
    }

}





 ?>