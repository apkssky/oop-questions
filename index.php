<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Bangun Datar</title>
<style>
p {
  font-size: 12px;
}
.alert {
  color: red;
}
</style>
</head>

<body>

  <form action="" method="POST">
    <h1>Persegi</h1>
      <input type="text" name="sisi" placeholder="Sisi">
      <select name="calInserted">
        <option value="luaspersegi">Luas</option>
        <option value="kllpersegi">Keliling</option>
      </select>
      <button type="submit">Calculate</button>
      
  </form>
  <form action="" method="POST">
  <h1>Persegi Panjang</h1>
      <input type="text" name="panjang" placeholder="Panjang">
      <input type="text" name="lebar" placeholder="Lebar">
      <select name="calInserted">
        <option value="luaspersegipanjang">Luas</option>
        <option value="kllpersegipanjang">Keliling</option>
      </select>
      <button type="submit">Calculate</button>
</form>
<form action="" method="POST">
  <h1>Lingkaran</h1>
      <input value="3.14" disabled>
      <input type="text" name="jarijari" placeholder="Jari-jari">
      <select name="calInserted">
        <option value="luaslingkaran">Luas</option>
        <option value="klllingkaran">Keliling</option>
      </select>
      <button type="submit">Calculate</button>
</form>
<form action="" method="POST">
  <h1>Belah ketupat</h1>
    <div id="ifYes" style="display: none">
      <input type="text" name="sisi" placeholder="Sisi">
    </div>
    <div id="ifNo" style="display: block">
      <input type="text" name="d1" placeholder="Diagonal1">
      <input type="text" name="d2" placeholder="Diagonal2">
    </div>
      <select name="calInserted" onchange="showSisibk(this)">
        <option value="luasbelahketupat">Luas</option>
        <option value="kllbelahketupat">Keliling</option>
      </select>
      <button type="submit">Calculate</button>
</form>
<form action="" method="POST">
  <h1>Segitiga</h1>
    <div id="ifYa" style="display: none">
      <input type="text" name="sisi" placeholder="Sisi">
    </div>
    <div id="ifTdk" style="display: block">
      <input type="text" name="alas" placeholder="Alas">
      <input type="text" name="tinggi" placeholder="Tinggi">
    </div>
      <select name="calInserted" onchange="showSisisg(this)">
        <option value="luassegitiga">Luas</option>
        <option value="kllsegitiga">Keliling</option>
      </select>
      <button type="submit">Calculate</button>
</form>
<?php 
include_once 'func.php';
 ?>
<h4>ZUAM OOP-BangunDatar</h4>
</body>
<script>
    function showSisibk(that) {
        if (that.value == "kllbelahketupat") {
            document.getElementById("ifYes").style.display = "block";
            document.getElementById("ifNo").style.display = "none";
        } else if (that.value == "luasbelahketupat") {
          document.getElementById("ifNo").style.display = "block";
          document.getElementById("ifYes").style.display = "none";
        }
    };
    function showSisisg(that) {
        if (that.value == "kllsegitiga") {
            document.getElementById("ifYa").style.display = "block";
            document.getElementById("ifTdk").style.display = "none";
        } else if (that.value == "luassegitiga") {
          document.getElementById("ifTdk").style.display = "block";
          document.getElementById("ifYa").style.display = "none";
        }
    }
</script>
</html>
