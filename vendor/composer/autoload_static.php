<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit418128043ee5b816d0dacd07d6817c09
{
    public static $classMap = array (
        'BangunDatar' => __DIR__ . '/../..' . '/src/BangunDatar.php',
        'BelahKetupat' => __DIR__ . '/../..' . '/src/BelahKetupat.php',
        'Lingkaran' => __DIR__ . '/../..' . '/src/Lingkaran.php',
        'Luas' => __DIR__ . '/../..' . '/src/Luas.php',
        'Persegi' => __DIR__ . '/../..' . '/src/Persegi.php',
        'PersegiPanjang' => __DIR__ . '/../..' . '/src/PersegiPanjang.php',
        'Segitiga' => __DIR__ . '/../..' . '/src/Segitiga.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit418128043ee5b816d0dacd07d6817c09::$classMap;

        }, null, ClassLoader::class);
    }
}
